let modalLeft = document.getElementById('modal-Left');
let btnLeft = document.getElementById('btn-Left');
let closeLeft = document.querySelector('.close-left');

btnLeft.onclick = function () {
	modalLeft.style.display = "block";
}
closeLeft.onclick = function() {
	modalLeft.style.display = "none";
}

window.onclick = function(event) {
	if (event.target == modalLeft) {
		modalLeft.style.display = "none";
	}
}


let modalRight = document.getElementById('modal-Right');
let btnRight = document.getElementById('btn-Right');
let closeRight = document.querySelector('.close-right');

btnRight.onclick = function () {
	modalRight.style.display = "block";
}
closeRight.onclick = function() {
	modalRight.style.display = "none";
}

window.onclick = function(event) {
	if (event.target == modalRight) {
		modalRight.style.display = "none";
	}
}